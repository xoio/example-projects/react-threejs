Notes
==
Given that there was a design component involved, I'd like to think I tried my best in the short amount of time given.
That being said, there were of course some shortcuts taken, things that could be better, things I decided to skip in the interest of time, that sort of thing.
I just wanted to note some items you may likely find during your review that I am aware of, but unfortunately ran out of
time to try and make better.

* In the effort to be able to also add easing easily, GSAP was used for most of the animations. That being the case, it was a 
bit tricky to animate without causing weird hiccups depending on how fast someone was scrolling. I believe I have a reasonably good solution 
but that all being said, it is advised to scroll at a reasonable pace.


* While I have used React before, it has not been something that has come up regularly for me; there may be some best
  practices being missed or could be better written. As I understood the assignment, there were no stack requirements
  but I figured that I may as well use what was listed/alluded to in the job description.


* For determining sections of the page, a library like Zustand, Redux,etc could have been used; in the interest of
  time and since the `<Model/>` component was pretty much the only area that needed to keep track of state, I decided to do things locally.


* The starting title text may flash in instead of animating(like you'll see on a reload) due to the loading of the model.


* Mobile is only somewhat supported; expect things to break if you resize.


* Loading page is a bit sparse, unfortunately.


Any detailed feedback would be extremely appreciated regardless of the outcome.
I'd still like to learn about what could be better and where I might stand to improve. 