This was a sample project I built as part of an interview process. Though I didn't make it in the end and this is a little rough around the edges, they liked it enough to give me a second interview. 

If you'd like to see it live, you can see it here

[https://react.xoio.co/](https://react.xoio.co/)

To dev
===
* `npm install`
* `npm start`
