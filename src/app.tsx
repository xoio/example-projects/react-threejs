import {createRoot} from "react-dom/client";
import Scene from "@gl/scene.tsx";
import DOM from "@dom/dom.tsx"
import "./styles/app.pcss"
import {Suspense} from "react";
import Loading from "@dom/loading.tsx";
import Observer from "@dom/observer.tsx";

createRoot(document.getElementById("APP")).render(



    <>
        <Suspense fallback={<Loading/>}>
            <Observer>
                <main>
                    <DOM/>
                    <Scene/>
                </main>
            </Observer>
        </Suspense>
    </>
)