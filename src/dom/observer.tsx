import {createContext, useContext, useEffect, useState} from "react";
import {ObserveItem, ObserverContents, WatchItem} from "../types/main";

export const ObserveContext = createContext<ObserverContents>(null)

export function useObserve() {
    return useContext(ObserveContext)
}

/**
 * A basic wrapper around [IntersectionObserver]. Uses [ObserverItem] to store the item to watch as well as the
 * callback to run when the observed item is found.
 * @param props any component to render underneath this one.
 */
export default function (props) {

    const observer = new IntersectionObserver(onObserve)

    // these are the items that the IntersectionObserver will be watching for
    const [items] = useState<Array<ObserveItem>>([])

    // these are any items outside the scope of the watched element that still needs to be signaled if intersection
    // occurs
    const [watchers] = useState<Array<WatchItem>>([])

    useEffect(() => {

        // subscribe all items
        // TODO do items need to be unsubscribed?
        items.forEach(itm => {
            observer.observe(itm.el)
        })

    }, [items])

    function onObserve(entries: any) {
        entries.forEach((itm: any) => {
            if (itm.isIntersecting) {
                // look through general observed items
                items.forEach((o_itm: ObserveItem) => {
                    // run main callback
                    if (o_itm.el === itm.target && !o_itm.wasInView) {
                        o_itm.wasInView = true
                        o_itm.onView(itm.isIntersecting)

                        // check to see if there are any watchers from outside the scope of the item
                        watchers.forEach((w, i) => {
                            if (w.id === o_itm.id && !w.onViewRan) {

                                w.onViewRan = true
                                w.onView()

                            }
                        })
                    }
                })

            } else {

                items.forEach((o_itm: ObserveItem) => {

                    // run the outro callback if the item was in view
                    if (o_itm.el === itm.target && o_itm.wasInView) {

                        o_itm.outView()
                        o_itm.wasInView = false


                        // check to see if there are any watchers
                        watchers.forEach((w, i) => {
                            if (w.id === o_itm.id && w.onViewRan) {

                                w.onViewRan = false
                                try {
                                    w.outView()
                                }catch(e){}

                            }
                        })
                    }
                })
            }
        })

    }

    return (
        <>

            <ObserveContext.Provider value={{
                items,
                watchers
            }}>
                {props.children}
            </ObserveContext.Provider>
        </>
    )
}