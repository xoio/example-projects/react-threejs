import {forwardRef, useEffect} from "react";
import  {useObserve} from "@dom/observer.tsx";

export type ContentProps = {
    classNames?: string
    observedItemId?: string
    observerClassName?: string
    children: any
}
export default forwardRef(function (props: ContentProps, ref: any) {

    const {
        // any classnames to assign to the element that encompasses the content.
        classNames = "",

        observedItemId = "",
        // the class name to use for the animation to trigger on/off observation
        observerClassName = "load"
    } = props
    const observe = useObserve()

    useEffect(() => {

        observe.items.push({
            el: ref.current,
            id: observedItemId,
            onView() {
                ref.current.classList.add(observerClassName)

            },
            outView() {
                ref.current.classList.remove(observerClassName)
            }
        })


    }, [props])
    return (
        <>
            <section className="content-section ">
                <div ref={ref} className={`content-body ${classNames}`}>
                    {props.children}
                </div>
            </section>
        </>
    )
})