export default function(){

    return(
        <>
            <footer>

                <div className={"foot-wrap"}>
                    <h1 className={"align-center"}>Elevated. Innovative.
                        Attention to details.</h1>

                    <h2 className={"align-center"}>
                        Our most premium headset. A whole new way to work, create and collaborate.
                        Starting at
                        $999999.99 USD
                    </h2>

                    <button>Order Today</button>
                </div>
            </footer>
        </>
    )
}