import {useRef} from "react";
import {useObserve} from "@dom/observer.tsx";
import strings from "@lib/strings.tsx";
import Content from "@dom/components/content.tsx";

export default function () {

    const observe = useObserve()
    let el = useRef<HTMLElement>()

    return (
        <>
            <Content
                observedItemId={"intro"}
                ref={el} classNames={`align-right intro-copy right-loading`}>
                <h1>{strings.intro1}</h1>
                <h2>{strings.intro3}</h2>
            </Content>

        </>
    )
}