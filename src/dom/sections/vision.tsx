import strings from "@lib/strings.tsx";
import {useRef} from "react";
import Content from "@dom/components/content.tsx";
export default function(props){

    let el = useRef<HTMLElement>()
    return(
        <>
           <Content
               observedItemId={"vision"}
               classNames={"vision-copy"} ref={el}>
               <h1>{strings.visionHeading}</h1>
               <h2>{strings.vision1}</h2>
           </Content>
        </>
    )
}