import {useObserve} from "@dom/observer.tsx";
import {useEffect, useRef} from "react";
import strings from "@lib/strings.tsx";
import {toArray} from "@lib/utils.ts";

export default function () {
    const observer = useObserve();

    let title_ref = useRef()

    useEffect(() => {

        let title = title_ref.current as HTMLElement

        if (title) {
            let timer = setInterval(() => {

                if (document.body.classList.contains("loaded")) {

                    setTimeout(()=>{
                        // @ts-ignore
                        let chunks: Array<HTMLElement> = toArray(title.children)

                        let numItems = chunks.length

                        // hardcoded to prevent use of getComputedStyle - 1.5s is set as the transition for the span elements
                        // inside the h1
                        let time = 1500

                        chunks.forEach(itm => {

                            itm.style.transitionDelay = `${Math.random() + 0.5}s`
                            itm.classList.add("load")

                        })

                        // reveal scroll once all characters are revealed
                        setTimeout(() => {
                            let dom = document.querySelector("#DOM")
                            dom.classList.remove("hide-scroll")
                        }, time + 400)

                    },1000)
                    clearInterval(timer)
                }

            })
        }
    }, [])

    return (
        <>
            <section className="content-section">
                <div className="title padding">
                    <h1 className={"preload"} ref={title_ref}>{strings.getChunkedTitle()}</h1>
                </div>
            </section>
        </>
    )
}