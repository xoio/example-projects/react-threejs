import {useRef} from "react";
import Content from "@dom/components/content.tsx";
import strings from "@lib/strings.tsx";

export default function(){

    let el = useRef<HTMLElement>()

    return(
        <>
            <Content
                observedItemId={"audio"}
                classNames={"audio align-right right-loading"} ref={el}>
                <h1>{strings.audioHeading}</h1>
                <h2>{strings.audio1}</h2>
            </Content>
        </>
    )
}