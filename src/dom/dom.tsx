import Title from "@dom/sections/title.tsx";
import Intro from "@dom/sections/intro.tsx";
import Vision from "@dom/sections/vision.tsx";
import Audio from "@dom/sections/audio.tsx";
import Footer from "@dom/sections/footer.tsx";
import {useEffect, useRef} from "react";
import {toArray} from "@lib/utils.ts";

/**
 * DOM content for the site.
 * @param props
 */
export default function (props) {

    const dom = useRef<HTMLElement>()

    useEffect(() => {

        // force scroll to the top in the event of a page refresh so InterSectionObserver can properly work.
        dom.current.scrollTo(0, 0)

    }, []);

    return (
        <>

            <section ref={dom} id="DOM" className={"hide-scroll"}>
                <Title/>
                <Intro/>
                <Vision/>
                <Audio/>
                <Footer/>
            </section>
        </>
    )

}