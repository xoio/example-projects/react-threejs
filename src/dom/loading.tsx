import {useProgress} from "@react-three/drei";
import {useEffect} from "react";
import {toArray} from "@lib/utils.ts";

export default function () {
    const {progress, errors, item} = useProgress()

    useEffect(() => {
        if (progress === 100) {
            document.body.classList.add("loaded")
            let el = toArray(document.querySelectorAll(".preload"))
            el.forEach(itm => {
                itm.classList.remove("preload")
            })
        }
    }, [progress]);

    return (
        <>

            <section id="loading">
                <h1>Loading</h1>
            </section>
        </>
    )
}