/**
 * Turns a [NodeList] into an Array instead for easier manipulation
 * @param list {NodeList}
 * @return the NodeList contents now as an array
 */
export function toArray(list: NodeList) {
    return Array.prototype.slice.call(list)
}

/**
 * Converts a value to radians
 * @param deg {number} the value in degrees
 * @return the value in radians
 */
export function toRadians(deg: number) {
    return (deg * Math.PI) / 180;
}

/**
 * Primarily to help with scrolling, scales scroll position value to fit between
 * a min/max and between a certain max range
 * @param pos {number} the current scroll position
 * @param scrollRange {number} the max range of how far to scale the scroll, ie - scaling happens for only this long
 * @param min {number} the min value of the range
 * @param max {number} the max value of the range
 *
 * @return the current scale value
 */
export function scaleScrollPosition(
    pos: number,
    scrollRange: number,
    min: number,
    max: number
) {

    const targetRange = max - min
    const pct = (pos - min) / scrollRange
    return clamp(min + pct * targetRange,min,max)
}

/**
 * Ensures a value lies in between a min and a max
 * @param value
 * @param min
 * @param max
 * @returns {*}
 */
export function clamp(value: number, min: number, max: number) {
    return min < max
        ? (value < min ? min : value > max ? max : value)
        : (value < max ? max : value > min ? min : value)
}
