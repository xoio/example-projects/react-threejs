export default {
    title: "MagicQuestSnapLens",

    /**
     * Returns title split into chunks separated by <span> elements
     */
    getChunkedTitle() {
        return this.title.split("").map((chunk, i) => {
            return (
                <span key={i}>{chunk}</span>
            )
        })
    },

    // general front view
    intro1: `Our most premium headset. A whole new way to work, create and collaborate. `,
    intro2: `MagicQuestSnapLens seamlessly blends digital content with your physical space.`,
    intro3: "With breakthrough high resolution mixed reality you can engage effortlessly with the virtual world while maintaining presence in your physical space in hi-def color. ",

    // when showing the sides
    audioHeading: "Our most advanced Spatial Audio system ever.",
    audio1: "Feel like you're part of the action with enhanced sound clarity, bass performance and a 40% louder volume range than competitors ",

    visionHeading: "A sophisticated sensor array.",
    vision1: "A pair of high-resolution cameras transmit over one billion pixels per second to the displays so you can see the world around you clearly. "
}