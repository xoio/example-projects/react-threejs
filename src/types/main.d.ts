// main type for Observer component
export type ObserverContents = {
    items: Array<ObserveItem>
    watchers: Array<WatchItem>
}

// the type for any items to observe. id is generally optional but useful if anything outside the scope of the observed
// item needs to be aware of when the observed item is reached
export type ObserveItem = {
    el: HTMLElement,
    id?: string
    onView: Function,
    outView?: Function,
    wasInView?: boolean
}

// type for anything that wants to watch an element outside its scope.
export type WatchItem = {
    id: string
    onViewRan?: boolean
    onView: Function
    outView?: Function
}
