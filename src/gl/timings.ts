import {toRadians} from "@lib/utils.ts";

export const scaleTarget = {
    intro: 25,
    vision: 25,
    audio: 35
}

export const rotationTarget = {
    intro: {
        x: (Math.PI / 2),
        y: 0,
        z: toRadians(45)
    },
    vision: {
        x: (Math.PI / 2),
        y: 0,
        z: 0
    },
    audio: {
        x: (Math.PI / 2) - toRadians(5),
        y: 0,
        z: toRadians(-113)
    }
}

export const positionTarget = {
    intro: {
        x: 0,
        y: 0,
        z: 0
    },
    vision: {
        x: 0,
        y: 0,
        z: 0
    },
    audio: {
        x: 3,
        y: -2,
        z: 0
    }
}
