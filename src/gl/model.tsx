import {useGLTF} from "@react-three/drei";
import {useEffect, useMemo, useRef} from "react";
import {useObserve} from "@dom/observer.tsx";
import {DoubleSide, Mesh, Plane, SpotLight, Vector3} from "three";
import {scaleScrollPosition, toRadians} from "@lib/utils.ts";
import {useFrame, useThree} from "@react-three/fiber";
import Sequencer from "@gl/sequencer.ts";

enum PAGE_SECTIONS {
    INTRO,
    VISION,
    AUDIO,
}

export default function (props) {

    const {nodes, materials} = useGLTF("/helmet.glb")
    useGLTF.preload("/helmet.glb")

    const observe = useObserve()

    const {scene, gl} = useThree()

    const mesh = useRef<Mesh>()
    const light = useRef<SpotLight>()

    const clip_plane = new Plane(new Vector3(0, -1, 0), -24)

    // TODO cloning temporarily in case there's time to try and do more
    const mat = useMemo(() => {
        let mat = materials.Material_MR.clone()

        mat.clippingPlanes = [clip_plane]

        // should be transparent
        mat.transparent = true
        mat.opacity = 1
        mat.side = DoubleSide
        return mat
    }, [])


    let section = null
    let scrollPos = 0
    let scalePos = 0
    let currentScale = 0
    let audioReached = false

    let sequencer: Sequencer

    ///////////// GENERAL SETUP /////////////
    useEffect(() => {
        sequencer = new Sequencer(mesh.current)

        observe.watchers.push({
            id: "intro",
            onView() {
                if (section === PAGE_SECTIONS.VISION) {
                    sequencer.visionIntro()
                }

                section = PAGE_SECTIONS.INTRO
            }
        })

        observe.watchers.push({
            id: "vision",
            onView() {
                if (section === PAGE_SECTIONS.INTRO) {
                    sequencer.introVision()
                } else {
                    sequencer.audioVision()
                }

                section = PAGE_SECTIONS.VISION
            }
        })

        observe.watchers.push({
            id: "audio",
            onView() {
                audioReached = true
                if (section === PAGE_SECTIONS.VISION) {
                    sequencer.visionAudio()
                }
                section = PAGE_SECTIONS.AUDIO

            },
            outView() {

                // only section after audio is footer; since the only way back is via audio, don't set
                // animation flag until the section is NOT the audio section
                if (section !== PAGE_SECTIONS.AUDIO) {
                    audioReached = false
                }
            }
        })

        scene.add(light.current.target)

    }, []);

    ///////////// HANDLE SCROLLING / CLIPPING /////////////
    useEffect(() => {
        //TODO not ideal but in the interest of time
        const el = document.querySelector("#DOM")
        let onScroll = (e) => {
            // there are about 4 sections so divide by 4 since things only need to animate within the first section
            const scrollRange = el.scrollHeight / 4
            scrollPos = scaleScrollPosition(el.scrollTop, scrollRange, -25, 25)
            scalePos = scaleScrollPosition(el.scrollTop, scrollRange, 1, 25)
        }

        window.addEventListener("scroll", onScroll)
        window.addEventListener("wheel", onScroll)

        return () => {
            window.removeEventListener("wheel", onScroll)
            window.removeEventListener("scroll", onScroll)
        }

    }, []);


    useFrame(({pointer}) => {
        const m = mesh.current

        let dx = (pointer.x * 50)
        let dy = (pointer.y * 50)

        light.current.target.position.x = dx;
        light.current.target.position.y = dy

        // if we've already hit the content, don't make any adjustments
        //if(contentReached) return

        if (scrollPos !== 0) {
            let s = scrollPos - clip_plane.constant
            let s2 = scalePos - currentScale
            clip_plane.constant += s * 0.02


            if (!audioReached) {
                currentScale += s2 * 0.02
                m.scale.set(currentScale, currentScale, currentScale)
            }
        }
    })

    return (
        <>
            <group {...props} dispose={null}>
                <spotLight
                    angle={toRadians(25)}
                    power={4000}
                    ref={light} position={[0, 0, 50]} distance={30} intensity={3000} color="lightyellow"/>
                <mesh
                    ref={mesh}
                    castShadow
                    receiveShadow
                    geometry={
                        //@ts-ignore
                        nodes["node_damagedHelmet_-6514"].geometry}
                    material={mat}
                    scale={[1, 1, 1]}
                    rotation={[(Math.PI / 2), 0, toRadians(45)]}
                />
            </group>
        </>
    )
}