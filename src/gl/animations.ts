import {gsap} from "gsap";
import {Material, Mesh} from "three";
import {toRadians} from "@lib/utils.ts"

const scaleTarget = {
    intro: 25,
    vision: 25,
    audio: 35
}

const rotationTarget = {
    intro: {
        x: (Math.PI / 2),
        y: 0,
        z: toRadians(45)
    },
    vision: {
        x: (Math.PI / 2),
        y: 0,
        z: 0
    },
    audio: {
        x: (Math.PI / 2) - toRadians(5),
        y: 0,
        z: toRadians(-113)
    }
}

const positionTarget = {
    intro: {
        x: 0,
        y: 0,
        z: 0
    },
    vision: {
        x: 0,
        y: 0,
        z: 0
    },
    audio: {
        x: 3,
        y: -2,
        z: 0
    }
}

/**
 * Builds animations for each section we scroll to. A little heavy-handed perhaps but much simpler to deal with
 * vs timelines
 * @param mat {Material} the material to animate - we want to animate the opacity
 * @param mesh {Mesh} the GLTF model
 */
export function animations(mat: Material, mesh: Mesh) {

    return {

        /**
         * Animates model to the intro position
         */
        toIntro(cb = () => {}) {

            let toComplete = 3
            let completed = 0

            gsap.to(mesh.scale, {
                x: scaleTarget.intro,
                y: scaleTarget.intro,
                z: scaleTarget.intro,
                duration: 1.5,
                ease: "power4.inOut",
                onComplete() {
                    completed += 1
                }
            })
            gsap.to(mesh.rotation, {
                ...rotationTarget.intro,
                duration: 1.2,
                ease: "power4.inOut",
                onComplete() {
                    completed += 1
                }
            })
            gsap.to(mat, {
                opacity: 1,
                duration: 2,
                onComplete() {
                    completed += 1
                }
            })

            let timer = setInterval(() => {
                if (completed === toComplete) {
                    cb()
                    clearInterval(timer)
                }
            })
        },

        /**
         * Animates model away from the intro position
         * @deprecated
         */
        outIntro() {
            gsap.to(mesh.scale, {
                x: 1,
                y: 1,
                z: 1,
            })
            gsap.to(mat, {
                opacity: 0
            })
        },

        /**
         * Animates from intro -> vision position
         */
        introVision(cb = () => {}) {
            gsap.fromTo(mesh.rotation, {
                ...rotationTarget.intro,
            }, {
                ...rotationTarget.vision,
                duration: 1.2,
                ease: "power4.inOut",
                onComplete() {
                    cb()
                }
            })
        },

        /**
         * Animation from vision -> intro
         */
        visionIntro(cb = () => {}) {
            gsap.fromTo(mesh.rotation, {
                ...rotationTarget.vision
            }, {
                ...rotationTarget.intro,
                duration: 1.2,
                ease: "elastic.inOut",
                onComplete(){
                    cb()
                }
            })
        },

        /**
         * Animation from vision -> audio
         */
        visionAudio(cb = () => {}) {
            let toComplete = 3
            let completed = 0

            gsap.fromTo(mesh.rotation, {
                ...rotationTarget.vision,
            }, {
                ...rotationTarget.audio,
                onComplete(){
                    completed += 1
                }
            })
            gsap.fromTo(mesh.position, {
                ...positionTarget.vision,
                ease: "power2.out"
            }, {
                ...rotationTarget.audio,
                onComplete(){
                    completed += 1
                }
            })
            gsap.to(mesh.scale, {
                x: scaleTarget.audio,
                y: scaleTarget.audio,
                z: scaleTarget.audio,
                duration: 1.2,
                ease: "power4.inOut",
                onComplete(){
                    completed += 1
                }
            })

            let timer = setInterval(() => {
                if (completed === toComplete) {
                    cb()
                    clearInterval(timer)
                }
            })
        },

        /**
         * Animation from audio -> vision
         */
        audioVision(cb = () => {}) {
            let toComplete = 3
            let completed = 0
            gsap.fromTo(mesh.rotation, {
                ...rotationTarget.audio
            }, {
                ...rotationTarget.vision,
                onComplete(){
                    completed += 1
                }
            })
            gsap.fromTo(mesh.position, {
                ...positionTarget.audio,
                ease: "elastic.inOut",
                duration: 0.2
            }, {
                ...rotationTarget.vision,
                onComplete(){
                    completed += 1
                }
            })
            gsap.fromTo(mesh.scale, {
                x: scaleTarget.audio,
                y: scaleTarget.audio,
                z: scaleTarget.audio,
            }, {
                x: scaleTarget.vision,
                y: scaleTarget.vision,
                z: scaleTarget.vision,
                onComplete(){
                    completed += 1
                }
            })
            let timer = setInterval(() => {
                if (completed === toComplete) {
                    cb()
                    clearInterval(timer)
                }
            })
        }
    }

}


/*

    This works in general, it's just reversing things is kind of a pain, hence the
    verbose method above.

    // build intro timeline
    intro.add(
        gsap.to(mat, {
            opacity: 1,
            duration: 1.8
        }),
    ).add(
        gsap.to(mesh.scale, {
            x: scaleTarget.vision,
            y: scaleTarget.vision,
            z: scaleTarget.vision,
            duration: 1.5,
            ease: "elastic.inOut"
        }),
        "-=1.8"
    ).add(
        gsap.to(mesh.rotation, {
            ...rotationTarget.intro,
            duration: 1.2,
            ease: "power4.inOut"
        }),
        "-=1.8"
    ).add(
        gsap.to(mesh.position, {
            ...positionTarget.intro,
            duration: 1.2,
            ease: "power4.inOut",
        }),
        "-=1.8"
    )


    vision.add(
        gsap.to(mesh.scale, {
            x: scaleTarget.vision,
            y: scaleTarget.vision,
            z: scaleTarget.vision,
            duration: 1.5,
            ease: "elastic.inOut"
        })
    ).add(
        gsap.to(mesh.rotation, {
            ...rotationTarget.vision,
            duration: 1.2,
            ease: "power4.inOut"
        }),
        "-=1.8"
    ).add(
        gsap.to(mesh.position, {
            ...positionTarget.vision,
            duration: 1.2,
            ease: "power4.inOut",
        }),
        "-=1.8"
    )

    audio.add(
        gsap.to(mesh.scale, {
            x: scaleTarget.audio,
            y: scaleTarget.audio,
            z: scaleTarget.audio,
            duration: 1.5,
            ease: "elastic.inOut"

        })
    ).add(
        gsap.to(mesh.rotation, {
            ...rotationTarget.audio,
            duration: 1.2,
            ease: "power4.inOut"
        }),
        "-=1.8"
    ).add(
        gsap.to(mesh.position, {
            ...positionTarget.audio,
            duration: 1.2,
            ease: "power4.inOut"
        }),
        "-=1.8"
    )

 */