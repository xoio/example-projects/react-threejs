import {Canvas} from "@react-three/fiber";
import Particles from "@gl/particles.tsx";
import Model from "@gl/model.tsx";

/**
 * GL content for the site.
 */
export default function () {

    return (
        <>


            <section id="GL">
                <Canvas
                    eventPrefix={"client"}
                    eventSource={document.querySelector("#APP") as HTMLElement}
                    camera={{fov: 75, position: [0, 0, 60]}}
                    onCreated={({ gl }) => {
                       gl.localClippingEnabled = true
                    }}>
                    <fog attach="fog" args={['white', 50, 190]}/>
                    <pointLight position={[0, 0, 30]} distance={100} intensity={800} color="lightblue"/>
                    <pointLight position={[0, 0, -30]} distance={100} intensity={800} color="lightblue"/>
                    <Model/>
                    <Particles/>
                </Canvas>
            </section>

        </>
    )

}