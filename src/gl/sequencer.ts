import {gsap} from "gsap";
import {Mesh} from "three";
import {rotationTarget, scaleTarget} from "@gl/timings.ts";

export default class Sequencer {

    _visionAudio: gsap.core.Timeline
    _introVision: gsap.core.Timeline
    _visionIntro: gsap.core.Timeline
    _audioVision: gsap.core.Timeline

    // the current running animation if any
    currentAnimation: gsap.core.Timeline = null

    constructor( mesh: Mesh) {
        this._buildIntroVision(mesh)
        this._buildVisionAudio(mesh)
        this._buildAudioVision(mesh)
        this._buildVisionIntro(mesh)
    }

    introVision() {

        this._checkForRunningAnimation(() => {
            // make sure to restart
            this._introVision.seek(0)
            this._introVision.play()
            this.currentAnimation = this._introVision
        })
    }

    visionAudio() {
        this._checkForRunningAnimation(() => {
            this._visionAudio.seek(0)
            this._visionAudio.play()
            this.currentAnimation = this._visionAudio
        })
    }

    audioVision() {
        this._checkForRunningAnimation(() => {
            this._audioVision.seek(0)
            this._audioVision.play()
            this.currentAnimation = this._audioVision
        })
    }

    visionIntro() {
        this._checkForRunningAnimation(() => {
            this._visionIntro.seek(0)
            this._visionIntro.play()
            this.currentAnimation = this._visionIntro
        })
    }

    /**
     * Checks to see if there is a currently running animation. If there is, make the animation skip to the end and
     * start running the next animation passed in through the callback function
     * @param cb {Function} the callback function
     */
    _checkForRunningAnimation(cb: Function) {
        if (this.currentAnimation) {
            // pause at the end
            this.currentAnimation.pause(this.currentAnimation.totalDuration())
            cb()
        } else {
            cb()
        }
    }

    /**
     * Builds the timeline to handle the animation from the intro section to the vision section
     * @param mesh {Mesh} the mesh object to manipulate
     */
    _buildIntroVision(mesh: Mesh) {
        const tl = gsap.timeline({
            paused: true
        })

        tl.add(
            gsap.fromTo(mesh.rotation, {
                ...rotationTarget.intro,
            }, {
                ...rotationTarget.vision,
                duration: 1.2,
                ease: "power4.inOut"
            })
        )
        tl.eventCallback("onComplete", () => {
            this.currentAnimation = null
        })


        this._introVision = tl
    }

    /**
     * Builds the timeline to handle the animation from the vision section to the audio section
     * @param mesh {Mesh} the mesh object to manipulate
     */
    _buildVisionAudio(mesh: Mesh) {
        const tl = gsap.timeline({
            paused: true
        })

        tl.add(
            gsap.to(mesh.rotation, {
                ...rotationTarget.audio,
                ease: "elastic.inOut",
                duration:1.6
            })
        ).add(
            gsap.to(mesh.scale, {
                x: scaleTarget.audio,
                y: scaleTarget.audio,
                z: scaleTarget.audio,
                duration: 1.2,
                ease: "power4.inOut"
            })
        )

        tl.eventCallback("onComplete", () => {
            this.currentAnimation = null
        })

        this._visionAudio = tl
    }

    /**
     * Builds the timeline to handle the animation from the audio section to the vision section
     * @param mesh {Mesh} the mesh object to manipulate
     */
    _buildAudioVision(mesh: Mesh) {
        const tl = gsap.timeline({
            paused: true
        })

        tl.add(
            gsap.to(mesh.rotation, {
                ...rotationTarget.vision
            })
        ).add(
            gsap.to(mesh.scale, {
                x: scaleTarget.vision,
                y: scaleTarget.vision,
                z: scaleTarget.vision,
                duration: 1.5,
                ease: "elastic.inOut"
            })
        )

        tl.eventCallback("onComplete", () => {
            this.currentAnimation = null
        })

        this._audioVision = tl
    }

    /**
     * Builds the timeline to handle the animation from the vision section to the intro section
     * @param mesh {Mesh} the mesh object to manipulate
     */
    _buildVisionIntro(mesh: Mesh) {
        const tl = gsap.timeline({
            paused: true
        })

        tl.add(
            gsap.to(mesh.rotation, {
                ...rotationTarget.intro,
                duration: 1.2,
                ease: "elastic.inOut"
            })
        )

        tl.eventCallback("onComplete", () => {
            this.currentAnimation = null
        })

        this._visionIntro = tl
    }
}
