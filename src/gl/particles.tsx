import {useEffect, useMemo, useRef} from "react";
import {InstancedMesh, MeshPhongMaterial, Object3D} from "three";
import {useFrame} from "@react-three/fiber";
import {gsap} from "gsap";

export default function (props) {

    const {
        num_particles = 400
    } = props


    let mesh = useRef<InstancedMesh>()
    let mat = useRef<MeshPhongMaterial>()
    let tmp = useMemo(() => new Object3D(), [])
    const particles = useMemo(() => {

        const data = []
        const positionOffset = 100

        for (let i = 0; i < num_particles; ++i) {
            const t = Math.random() * 100
            const factor = 20 + Math.random() * 100
            const speed = 0.01 + Math.random() / 200
            const xFactor = -50 + Math.random() * positionOffset
            const yFactor = -50 + Math.random() * positionOffset
            const zFactor = -50 + Math.random() * positionOffset

            data.push({t, factor, speed, xFactor, yFactor, zFactor, mx: 0, my: 0})
        }

        return data
    }, [])

    const offset = 0.5

    useEffect(() => {
        mat.current.opacity = 0
        mat.current.transparent = true

        setTimeout(() => {
            gsap.to(mat.current, {
                opacity: 1,
                duration: 1.5
            })
        }, 500)
    }, []);

    useFrame(() => {


        particles.forEach((particle, i) => {

            tmp.position.set(particle.x, particle.y, particle.z)
            tmp.updateMatrix()


            let {t, factor, speed, xFactor, yFactor, zFactor} = particle
            // There is no sense or reason to any of this, just messing around with trigonometric functions
            t = particle.t += speed / 10
            const a = Math.cos(t) + Math.sin(t * 1)
            const b = Math.sin(t) + Math.cos(t * 2)
            const s = Math.cos(t)
            particle.mx += 0.01;
            particle.my += 0.01;

            tmp.position.set(
                a + xFactor + Math.cos((t / offset) * factor) + (Math.sin(t * 1) * factor) / offset,
                b + yFactor + Math.sin((t / offset) * factor) + (Math.cos(t * 2) * factor) / offset,
                //  b + zFactor + Math.cos((t / offset) * factor) + (Math.sin(t * 3) * factor) / offset
                -50
            )

            tmp.position.x += 30
            tmp.position.y += 30
            tmp.position.z += 30

            tmp.scale.set(s, s, s)
            tmp.rotation.set(s * 5, s * 5, s * 5)
            tmp.updateMatrix()

            mesh.current.setMatrixAt(i, tmp.matrix)
            mesh.current.rotation.z += 0.000004;
            mesh.current.instanceMatrix.needsUpdate = true

        })

    })

    return (
        <>
            <pointLight distance={40} intensity={2800} color="lightblue"/>
            <instancedMesh
                ref={mesh} args={[null, null, num_particles]}>
                <dodecahedronGeometry args={[0.5, 0]}/>
                <meshPhongMaterial
                    ref={mat}
                    color={"white"}/>
            </instancedMesh>
        </>
    )
}