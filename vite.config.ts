import {defineConfig} from "vite";
import react from "@vitejs/plugin-react"
import path from "path";

export default defineConfig({
    plugins: [react()],
    resolve: {
        alias: {
            "@gl": path.resolve(__dirname, "src/gl"),
            "@dom": path.resolve(__dirname, "src/dom"),
            "@lib": path.resolve(__dirname, "src/lib"),
        }
    }
})